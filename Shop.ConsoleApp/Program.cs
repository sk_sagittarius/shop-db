﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet shopDB = new DataSet("ShopDB");
            DataTable orders = new DataTable("Orders");
            DataTable customers = new DataTable("Customers");
            DataTable employees = new DataTable("Employees");
            DataTable orderDetails = new DataTable("OrderDetails");
            DataTable products = new DataTable("Products");

            InitOrdersTable(ref orders);
            InitCustomersTable(ref customers);
            InitEmployeesTable(ref employees);
            InitOrderDetailsTable(ref orderDetails);
            InitProductsTable(ref products);

            ForeignKeyConstraint FK_Customer = new ForeignKeyConstraint(customers.Columns["Id"], orders.Columns["CustomerId"])
            {
                DeleteRule = Rule.Cascade,
                UpdateRule = Rule.Cascade
            };
            ForeignKeyConstraint FK_Product = new ForeignKeyConstraint(products.Columns["Id"], orderDetails.Columns["ProductId"])
            {
                DeleteRule = Rule.Cascade,
                UpdateRule = Rule.Cascade
            };
            ForeignKeyConstraint FK_Order = new ForeignKeyConstraint(orders.Columns["Id"], orderDetails.Columns["OrderId"])
            {
                DeleteRule = Rule.Cascade,
                UpdateRule = Rule.Cascade
            };
            ForeignKeyConstraint FK_OrderDetails = new ForeignKeyConstraint(orderDetails.Columns["Id"], employees.Columns["OrderDetailsId"])
            {
                DeleteRule = Rule.Cascade,
                UpdateRule = Rule.Cascade
            };

            orders.Constraints.Add(FK_Customer);
            orderDetails.Constraints.Add(FK_Product);
            orderDetails.Constraints.Add(FK_Order);
            employees.Constraints.Add(FK_OrderDetails);

            shopDB.Tables.AddRange(new DataTable[]
            {
                customers,
                products,
                orders,
                orderDetails,
                employees
            });

            //shopDB.WriteXml("shop.xml");
            shopDB.WriteXmlSchema("shop2.xml");
        }

        private static void InitProductsTable(ref DataTable products)
        {
            DataColumn id = new DataColumn("Id", typeof(int))
            {
                AllowDBNull = false,
                AutoIncrement = true,
                AutoIncrementSeed = 1,
                AutoIncrementStep = 1,
                ReadOnly = true,
                Unique = true
            };

            DataColumn name = new DataColumn("Name", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 100
            };

            DataColumn price = new DataColumn("Price", typeof(int))
            {
                AllowDBNull = false
            };

            products.Columns.AddRange(new DataColumn[]
            {
                id,
                name,
                price
            });

            products.PrimaryKey = new DataColumn[] { products.Columns["Id"] };
        }
    
        private static void InitOrderDetailsTable(ref DataTable orderDetails)
        {
            DataColumn id = new DataColumn("Id", typeof(int))
            {
                AllowDBNull = false,
                AutoIncrement = true,
                AutoIncrementSeed = 1,
                AutoIncrementStep = 1,
                ReadOnly = true,
                Unique = true
            };

            DataColumn productId = new DataColumn("ProductId", typeof(int))
            {
                AllowDBNull = false
            };

            DataColumn orderId = new DataColumn("OrderId", typeof(int))
            {
                AllowDBNull = false
            };

            DataColumn orderCommonPrice = new DataColumn("OrderCommonPrice", typeof(int))
            {
                AllowDBNull = false
            };

            DataColumn orderQuantity = new DataColumn("OrderQuantity", typeof(int))
            {
                AllowDBNull = false,
                DefaultValue = 1
            };

            orderDetails.Columns.AddRange(new DataColumn[]
            {
                id,
                productId,
                orderId,
                orderCommonPrice,
                orderQuantity
            });

            orderDetails.PrimaryKey = new DataColumn[] { orderDetails.Columns["Id"] };
        }

        private static void InitEmployeesTable(ref DataTable employees)
        {
            DataColumn id = new DataColumn("Id", typeof(int))
            {
                AllowDBNull = false,
                AutoIncrement = true,
                AutoIncrementSeed = 1,
                AutoIncrementStep = 1,
                ReadOnly = true,
                Unique = true
            };

            DataColumn firstName = new DataColumn("FirstName", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 100
            };
            DataColumn lastName = new DataColumn("LastName", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 100
            };

            DataColumn orderDetailsId = new DataColumn("OrderDetailsId", typeof(int))
            {
                AllowDBNull = false
            };

            employees.Columns.AddRange(new DataColumn[]
            {
                id,
                firstName,
                lastName,
                orderDetailsId
            });

            employees.PrimaryKey = new DataColumn[] { employees.Columns["Id"] };
        }

        private static void InitCustomersTable(ref DataTable customers)
        {
            DataColumn id = new DataColumn("Id", typeof(int))
            {
                AllowDBNull = false,
                AutoIncrement = true,
                AutoIncrementSeed = 1,
                AutoIncrementStep = 1,
                ReadOnly = true,
                Unique = true
            };

            DataColumn firstName = new DataColumn("FirstName", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 100
            };
            DataColumn lastName = new DataColumn("LastName", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 100
            };

            DataColumn email = new DataColumn("Email", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 50
            };

            DataColumn phoneNumber = new DataColumn("PhoneNumber", typeof(string))
            {
                AllowDBNull = true,
                MaxLength = 20
            };

            DataColumn adress = new DataColumn("Adress", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 100
            };

            DataColumn city = new DataColumn("City", typeof(string))
            {
                AllowDBNull = false,
                MaxLength = 20
            };

            customers.Columns.AddRange(new DataColumn[]
            {
                id,
                firstName,
                lastName,
                email,
                phoneNumber,
                adress,
                city
            });

            customers.PrimaryKey = new DataColumn[] { customers.Columns["Id"] };
        }

        private static void InitOrdersTable(ref DataTable orders)
        {
            DataColumn id = new DataColumn("Id", typeof(int))
            {
                AllowDBNull = false,
                AutoIncrement = true,
                AutoIncrementSeed = 1,
                AutoIncrementStep = 1,
                ReadOnly = true,
                Unique = true
            };

            //DataColumn status = new DataColumn("Status", typeof(string))
            //{
            //    AllowDBNull = false,
            //    MaxLength = 20
            //};

            DataColumn date = new DataColumn("Date", typeof(DateTime))
            {
                DefaultValue = DateTime.Now.ToShortTimeString()
            };

            DataColumn customerId = new DataColumn("CustomerId", typeof(int))
            {
                AllowDBNull = false
            };

            orders.Columns.AddRange(new DataColumn[]
            {
                id,
                date,
                customerId
            });

            orders.PrimaryKey = new DataColumn[] { orders.Columns["Id"] };
        }
    }
}
